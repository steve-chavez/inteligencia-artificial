;; Funciones
(defun pasos-hanoi(numero-discos origen destino aux)
  "devuelve los pasos en una lista de listas compuestas por 2 elementos: el origen y destino"
  (case numero-discos
    (0 nil)
    (otherwise
      (append
        (pasos-hanoi (- numero-discos 1) origen aux destino)
        (list (list origen destino))
        (pasos-hanoi (- numero-discos 1) aux destino origen)))))

;; Pruebas
(print
  (if
    (and
      (equal
        (pasos-hanoi 3 'a 'b 'c)
        '((A B) (A C) (B C) (A B) (C A) (C B) (A B)))
      (equal
        (pasos-hanoi 4 'a 'c 'b)
        '((A B) (A C) (B C) (A B) (C A) (C B) (A B) (A C) (B C) (B A) (C A) (B C) (A B) (A C) (B C))))
    "hanoi: prueba exitosas"
    "hanoi: prueba fallidas"))

;; Se puede usar la sig funcion para visualizar mejor los pasos
(defun muestra-resolucion-hanoi(numero-discos origen destino aux)
  (let
    ((conteo 0))
    (loop for x in (pasos-hanoi numero-discos origen destino aux) do
      (setq conteo (+ conteo 1))
      (format t "~% ~d. ~s -> ~s" conteo (car x) (cadr x)))))

;;(muestra-resolucion-hanoi 4 'a 'c 'b)
;;Esto mostrará:
;;1. A -> B
;;2. A -> C
;;3. B -> C
;;4. A -> B
;;5. C -> A
;;6. C -> B
;;7. A -> B
;;8. A -> C
;;9. B -> C
;;10. B -> A
;;11. C -> A
;;12. B -> C
;;13. A -> B
;;14. A -> C
;;15. B -> C

