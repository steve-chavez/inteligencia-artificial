;; Funcion
(defun mcd(a b)
  (cond
    ((= b 0) a)
    ((> b 0) (mcd b (mod a b)))))


;; Pruebas
(print
  (if
    (and
      (= (mcd 57 23)  1)
      (= (mcd 23 1)   1)
      (= (mcd 1 0)    1)
      (= (mcd 9 12)   3)
      (= (mcd 125 35) 5))
    "mcd: pruebas exitosas"
    "mcd: pruebas fallidas"))
