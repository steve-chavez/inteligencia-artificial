
% Iterative factorial

%factorial(N, F) :- factorial(O, N, 1, F).

%factorial(I, N, T, F) :- I < N, I1 is I + 1, T1 is T * I1, factorial(I1, N, T1, F).

%factorial(N, N, F, F).
%

is_what.

%member(1, [1,2,3]).

%https://stackoverflow.com/questions/40890781/swipl-how-to-reload-file
%make. % reload file
%apropos(make). % doc

%https://stackoverflow.com/a/5223954
%[user]. % For inputing knowledge base from stdin

%2 = 3.
%=(mia, vincent).
%mia = mia.
%mia = X.
%
% [Head|Tail]  =  [mia,  vincent,  jules,  yolanda].
% [X,Y  |  W]  =  [[],  dead(z),  [2,  [b,  c]],  [],  Z]. 
%
% [_,X,_,Y|_]  =  [[],  dead(z),  [2,  [b,  c]],  [],  Z]. 
%
% [_,_,[_|X]|_]  = [[],  dead(z),  [2,  [b,  c]],  [],  Z,  [2,  [b,  c]]]. 
               
%8 is 6 + 2.
%X is 4 * 5.
%
%X  is  +(3,2).

%is(X,+(3,2)).
%
%
len([],0). 
len([_|T],N) :- len(T,X),  N  is  X + 1.

accLen([_|T],A,L)  :-    Anew  is  A+1,  accLen(T,Anew,L). 
accLen([],A,A).

leng(List,Length)  :-  accLen(List,0,Length).

%4=\=5.
%4 \=5.
%From https://stackoverflow.com/questions/8523608/what-is-the-logical-not-in-prolog/8523825#8523825
%dif(4, 5).
%
%if else, from https://stackoverflow.com/a/6314926
%3 < 4 -> true; false.

%accMax([H|T],A,Max)  :- 
     %H  >  A 
     %accMax(T,H,Max). 

%accMax([H|T],A,Max)  :- 
     %H  =<  A, 
     %accMax(T,A,Max). 

accMax([H|T],A,Max)  :- 
  H  >  A ->
    accMax(T,H,Max);
    accMax(T,A,Max).
accMax([],A,A).

max(List,Max)  :- 
  [H|_]  =  List, 
  accMax(List,H,Max).

enjoys(vincent,X)  :-  not(big_kahuna_burger(X)). 
enjoys(vincent,X)  :-  burger(X). 

burger(X)  :-  big_mac(X). 
burger(X)  :-  big_kahuna_burger(X). 
burger(X)  :-  whopper(X). 

big_mac(a). 
big_kahuna_burger(b). 
big_mac(c). 
whopper(d).

%not(false).
%not(true).
%
%From http://learnprolognow.org/lpnpage.php?pagetype=html&pageid=lpn-htmlse48
%listing.
%assert(happy(vincent)).
%retract(happy(vincent)).
%
% Red vs green cut https://stackoverflow.com/questions/25962835/prolog-differences-between-red-cut-and-green-cut

dog(john).
dog(ben).

% execute this and output this right away when I open it in the console
%  This will write each successful query for dog(X)
%:- forall(dog(X), (write(X), nl)).
%
%between(0, 10, X).
%X = 0 ;
%X = 1 ;
%X = 2 ;
%X = 3 ;
%X = 4 ;
%X = 5 ;
%X = 6 ;
%X = 7 ;
%X = 8 ;
%X = 9 ;
%X = 10.
%
%between in pure prolog, from https://stackoverflow.com/questions/18337235/can-you-write-between-3-in-pure-prolog/18338089
bet(N, M, K) :- N =< M, K = N.
bet(N, M, K) :- N < M, N1 is N+1, bet(N1, M, K).

%bet(N, M, K) :- N < M, K = N.
%bet(N, M, K) :- N == M, !, K = N.
%bet(N, M, K) :- N < M, N1 is N+1, bet(N1, M, K).
%
%Which couple of numbers between 0 and 10 are such that the sum is 10? From https://stackoverflow.com/questions/28436385/find-combinations-of-integers-that-add-to-a-sum
%between(0, 10, X), between(0, 10, Y), plus(X, Y, 10).
%
%Use infinite or inf for infinite
%
% Prolog backtracking vs rete backtracking https://stackoverflow.com/questions/48486482/prolog-backtracking-vs-rete-backtracking
%
% The Rete Matching Algorithm (2002) (drdobbs.com) https://news.ycombinator.com/item?id=11364718
%
% https://en.wikipedia.org/wiki/SLD_resolution
%
% https://stackoverflow.com/questions/6060268/prolog-count-the-number-of-times-a-predicate-is-true
% aggregate_all for counting number of times
%
% existe_camino(X, Y, Y) :- arista(X, Y, _), !.
% existe_camino(X, Y, C) :- arista(X, Z, _), existe_camino(Z, Y, C1), string_concat("->", C1, C2), string_concat(Z, C2, C).
%
% https://stackoverflow.com/questions/29938961/prolog-avoid-infinite-loop
%
% How to find path without cycles in graph
% https://stackoverflow.com/questions/21161624/define-graph-in-prolog-edge-and-path-finding-if-there-is-a-path-between-two-ve
%
% Here's how to find path https://www.cpp.edu/~jrfisher/www/prolog_tutorial/2_15.html
%
% Setof demo
% http://www.cse.unsw.edu.au/~billw/dictionaries/prolog/setof.html
%
% Shortest path
% https://www.cpp.edu/~jrfisher/www/prolog_tutorial/2_15A.pl
%
% Family tree
% https://github.com/Anniepoo/prolog-examples/blob/master/familytree.pl
