
%03 Grafo

% Base de conocimiento

arista("1", "4", 13). 
arista("1", "7", 2). 
arista("2", "1", 1). 
arista("3", "1", 25). 
arista("3", "2", 2). 
arista("3", "5", 30). 
arista("5", "2", 5). 
arista("5", "6", 4). 
arista("5", "8", 14). 
arista("6", "3", 11). 
arista("6", "9", 9). 
arista("7", "4", 12). 
arista("7", "6", 17). 
arista("7", "10", 8). 
arista("8", "9", 3). 
arista("8", "11", 6). 
arista("9", "5", 15). 
arista("10", "9", 8). 
arista("11", "10", 7). 

existe_camino(A,B,Camino) :- recorrer(A,B,[A],R), reverse(R,Camino).

recorrer(A,B,P,[B|P]) :- arista(A,B, _).
recorrer(A,B,Visitados,Camino) :-
  arista(A,X, _),           
  X \== B,
  not(member(X,Visitados)),
  recorrer(X,B,[X|Visitados],Camino). 

existe_camino(A, B) :- existe_camino(A, B, R), R \== [].

nodos_conectados(X, Nodo, Costo) :- arista(X, Nodo, Costo).

tiene_arista(X) :- arista(X, _, _).

camino_con_salto(X, Y, Z, Costo) :- arista(X, Y, C1), arista(Y, Z, C2), Costo is C1 + C2.

%a)
%?- existe_camino("5", "10").
%true.

%b)
%nodos_conectados("5", Nodo, Costo).
%Nodo = "2",
%Costo = 5 ;
%Nodo = "6",
%Costo = 4 ;
%Nodo = "8",
%Costo = 14.

%c)
%?- tiene_arista("10").                                                                                                                 
%true.
%?- tiene_arista("4").                                                                                                                  
%false.

%d)
%?- camino_con_salto("6", "3", "2", Costo).
%Costo = 13 .
%?- camino_con_salto("9", "5", "6", Costo).
%Costo = 19 .

%e)
%?- existe_camino("1", "11", Camino).                                                                                                   
%Camino = ["1", "7", "6", "3", "5", "8", "11"] ;
%Camino = ["1", "7", "6", "9", "5", "8", "11"] ;
%Camino = ["1", "7", "10", "9", "5", "8", "11"] ;

%f)
