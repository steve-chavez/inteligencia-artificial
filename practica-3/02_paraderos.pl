
%02 Paraderos

conexion(unmsm, grau).
conexion(grau, javier_prado).
conexion(javier_prado, atocongo).
conexion(atocongo, villa_maria).

antes(A, B) :- conexion(A, B).
antes(A, B) :- conexion(A, X), antes(X, B).

despues(A, B) :- conexion(B, A).
despues(A, B) :- conexion(B, X), despues(A, X).

% Pruebas
%?- antes(unmsm, grau).
%true .
%?- antes(unmsm, atocongo).
%true .
%?- antes(villa_maria, javier_prado).
%false.
%?- despues(grau, unmsm).
%true .
%?- despues(atocongo, javier_prado).
%true .
%?- despues(unmsm, javier_prado).
%false.


subieron(unmsm, [juan, pedro]).
subieron(grau, [carlos, ana]).
subieron(javier_prado, [maria]).
subieron(atocongo, [jose]).

% Prueba
%?- subieron(unmsm, L).
%L = [juan, pedro].

bajaron(atocongo, [pedro, ana]).
bajaron(villa_maria, [carlos, maria, jose]).
bajaron(javier_prado, [juan]).

% Prueba
%?- subieron(unmsm, L).
%L = [juan, pedro].
