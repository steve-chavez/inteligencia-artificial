
%04 Recursividad

%a)
suma_N_numeros(0, 0) :- !.
suma_N_numeros(N, X) :- N1 is N - 1, suma_N_numeros(N1, X1), X is X1 + N.

%Pruebas
%?- suma_N_numeros(5, X).                                                                                                               
%X = 15.

%?- suma_N_numeros(10, X).                                                                                                              
%X = 55.

%b)
potencia_de_2(0, 1) :- !.
potencia_de_2(I, X) :- I1 is I - 1, potencia_de_2(I1, X1), X is 2*X1.

%Pruebas
%?- potencia_de_2(3, X).
%X = 8.

%?- potencia_de_2(4, X).
%X = 16.

%c)
robot_pasos(Metros, PasosDe1, PasosDe2) :- 
  between(0, 100, X), 
  between(0, 100, Y), 
  PasosDe1 is X,
  PasosDe2 is Y,
  Metros is 1*PasosDe1 + 2*PasosDe2.

%Pruebas
%robot_pasos(6, PasosDe1, PasosDe2).
%PasosDe1 = 0,
%PasosDe2 = 3 ;
%PasosDe1 = PasosDe2, PasosDe2 = 2 ;
%PasosDe1 = 4,
%PasosDe2 = 1 ;
%PasosDe1 = 6,
%PasosDe2 = 0 ;

robot_formas_recorrido(Metros, NroFormas) :- aggregate_all(count, robot_pasos(Metros, _, _), NroFormas).

%Pruebas
%robot_formas_recorrido(6, NroFormas).
%NroFormas = 4.
