
%01 Arbol genealogico

hombre(tomas).
hombre(jose).
hombre(jaime).
mujer(clara).
mujer(isabel).
mujer(ana).
mujer(patricia).

progenitor(clara, jose).
progenitor(tomas, jose).
progenitor(tomas, isabel).
progenitor(jose, ana).
progenitor(jose, patricia).
progenitor(patricia, jaime).

% 1.1
% a)
%?- progenitor(X, patricia).
%X = jose.

% b)
%?- progenitor(isabel, X), hombre(X).                                                                                                   
%false.
%?- progenitor(isabel, X), mujer(X).                                                                                                    
%false.

% c)
%?- hombre(X), progenitor(X,Y), progenitor(Y, isabel).
%false.

% d)
%?- progenitor(patricia, X).
%X = jaime.


% 1.2
% a)
es_madre(X) :- progenitor(X, _), mujer(X).

% b)
es_padre(X) :- progenitor(X, _), hombre(X).

% c)
es_hijo(X) :- progenitor(_, X), hombre(X).

% d)
hermana_de(X, Y) :- mujer(X), progenitor(P, X), progenitor(P, Y), X \= Y.

% e)
abuelo_de(X,Y) :- hombre(X), progenitor(X,P), progenitor(P,Y).
abuela_de(X,Y) :- mujer(X), progenitor(X,P), progenitor(P,Y).

% f)
hermanos(X,Y) :-  progenitor(P,X),progenitor(P,Y), X \= Y.

% g)
tia(X,Y) :- hermana_de(X, M), not(hombre(M)), progenitor(M, Y).

% 1.3

predecesor(X,Y) :- progenitor(Y,X).
predecesor(X,Y) :- progenitor(Y,Z), predecesor(X,Z).

% a)
sucesor(X, Y) :- predecesor(Y, X).

% b)
%?- sucesor(clara, X).
%X = jose ;
%X = ana ;
%X = patricia ;
%X = jaime ;
