;; Some lisp examples
(defun hello ()
  (write-line "What is your name?")
  (let ((name (read-line)))
    (format t "Hello, ~A.~%" name)))

(append '(Pat Kim) '(Robin Sandy))

(let
  ((zebra "stripes"))
  (print zebra))


(set 'x (if (= 1 2) 3 4))

; set quoted
(setq y 3 z 5)

; a and b get global scopes
(defun add_using_setq ()
  (setq a 3)  ; a never existed before , but still I'm able to assign value, what is its scope?
  (setq b 4)  ; b never existed before, but still I'm able to assign value, what is its scope?
  (+ a b))

; x and y get local scopes
(defun add_using_let ( )
  (let ((x 3) (y 4)) ; creating variables x and y
     (setq x 5)
     (setq y 10)
     (+ x y)))

(progv '(a b c) '(1 2 3) (+ a b c))

(let ((a 2)(b 3)) (+ a b))

(apply '+ '(1 2 3))

(eval (cons '+ '(2 3)))

(prog1 1 2)

(prog2 1 2)

(progn 1 2)

(if t 1 2)

;; Doesn't exist in sbcl (ifn t 1 2)

(when t 3)

(unless nil 3)

;; reminds me of guards in haskell
(defun notas(n)
  (cond ((< n 5) 'suspenso)
        ((and (>= n 5) (< n 8) 'aprobado))
        ((eq n 8) 'notable)
        (t 'sobresaliente) ))

;; though with the difference that the return is equal to the last true condition
(defun notas(n)
  (cond ((< n 5) 'suspenso)
        ((< n 7) 'aprobado)
        ((< n 9) 'notable)
        ((= n 10) 'sobresaliente) ))

(eq (notas 6) 'aprobado)
t

;; reminds me of pattern matching in haskell
(defun pp(n)
  (case n
    (1 'primero)
    (2 'segundo)
    ((3 4 5) 'posterior)
    (otherwise 'cualquiera)))

(loop (print "1") (print "2") (return 'fin))

(let ((a 10))
  (loop
   (setq a (+ a 1))
   (write a)
   (terpri)
   (when (> a 17) (return a))))

(loop for x in '(tom dick harry)
   do (format t " ~s" x))

(let ((x 10)) (loop while (>= x 0) do (print x) (setq x (- x 1))))

(loop for x from 0 upto 9 do 
  (loop for y from 0 upto 9 do 
    (format t " ~d ~d" x y)))

;; returns (+ x y)
(do
   ((x 0 (+ 2 x)) (y 20 ( - y 2)))
   ((= x y)(- x y))
   (format t "~% x = ~d  y = ~d" x y))

(dotimes (n 11)
   (print n) (prin1 (* n n)))

(mapc 'print '(a (b c) d))

(maplist 'append '(1 2 3 4))

(mapcar (lambda(x) (+ 1 x)) '(1 2 3 4))

(= 2 2 2)
(/= 2 3 4)
(> 4 3 2)

(first '(a b c))
(car '(a b c))

(cdr '(a b c))
(rest '(a b c))

(cadr '(a b c))
(second '(a b c))

(caddr '(a b c))
(third '(a b c))

(cadddr '(a b c d))
(fourth '(a b c d))

(last '(a b c d))

(length '(a b c))

(make-list 3 :initial-element 3)

(acons 'a 1 '((b . 2)(c . 3)))

(defun factorial(n)
  (cond
    ((<= n 1) 1)
    (t (* n (factorial (- n 1))))))

(trace factorial)

(factorial 5)

(untrace factorial)

(step (factorial 3)) ;; type ?, but not sure how to use, every command exits the function

(defun square(x) "A square func" (* x x))

(describe 'square)

(inspect 'square)

(sort '(3 1 10 11) '<)

(defun test (a &rest b) b)

(test 1 2 3 4)

(defmacro lcomp (expression for var in list conditional conditional-test)
  ;; create a unique variable name for the result
  (let ((result (gensym)))
    ;; the arguments are really code so we can substitute them
    ;; store nil in the unique variable name generated above
    `(let ((,result nil))
       ;; var is a variable name
       ;; list is the list literal we are suppose to iterate over
       (loop for ,var in ,list
            ;; conditional is if or unless
            ;; conditional-test is (= (mod x 2) 0) in our examples
            ,conditional ,conditional-test
            ;; and this is the action from the earlier lisp example
            ;; result = result + [x] in python
            do (setq ,result (append ,result (list ,expression))))
           ;; return the result
       ,result)))

(defun range-helper (x)
  (if (= x 0)
      (list x)
      (cons x (range-helper (- x 1)))))

(defun range (x)
  (reverse (range-helper (- x 1))))

(lcomp x for x in (range 10) if (= (mod x 2) 0))

;; local functions
(defun smth(x)
  (let
    ((plus-2 (lambda (n)(+ n 2))))
    (print (funcall plus-2 x))))

(smth 2)

((lambda (x) (+ x 1)) 1)

;; get command line args
(format t "~&~S~&" sb-ext:*posix-argv*)

;; build an self contained executable
;; https://lispcookbook.github.io/cl-cookbook/scripting.html#with-sbcl

;; when using case quit symbols don't quote
;; https://stackoverflow.com/questions/43210454/quoting-match-clause-in-case

;; defun with many steps
(defun verbose-sum (x y)
  "Sum any two numbers after printing a message."
  (format t "Summing ~d and ~d.~%" x y)
  (+ x y))

(defstruct reina
   jugador
)

(setq r (make-reina :jugador 'blanco))

(eq (reina-jugador r) 'blanco)

;; http://www.gigamonkeys.com/book/a-few-format-recipes.html
;; https://en.wikipedia.org/wiki/Format_(Common_Lisp)
(let ((groceries '((eggs bread) (butter carrots))))
 (format t "a b c ~% ~{~{~A~^, ~}~}.~%" groceries)         ; Prints in uppercase
 (format t "~@(~{~A~^, ~}~).~%" groceries))

(concatenate 'string "hello" "world")

; mutate nested list
;(defun actualizar-tablero (pos celda tablero)
  ;(let* ((fila (nth (posicion-x pos) tablero)))
    ;(setf (nth (posicion-y pos) fila) celda)
    ;(setf (nth (posicion-x pos) tablero) fila)
    ;tablero))

;; trying to not mutate list
;(defun actualizar-tablero (pos celda tablero)
  ;(let* ((old-fila (nth (posicion-x pos) tablero))
         ;(new-fila (append (subseq old-fila (- (posicion-y pos) 1)) '(celda) (subseq old-fila (+ (posicion-y pos) 1) 9))))
    ;(list (subseq tablero (- (posicion-x pos) 1)) new-fila (subseq tablero (+ (posicion-x pos) 1) 9))))

(loop for i in '(bird 3 4 turtle (1 . 4) horse cat) 
      when (symbolp i) collect i)

;; http://www.gigamonkeys.com/book/loop-for-black-belts.html
(loop for i from 1 upto 10 collect i)

; http://clhs.lisp.se/Body/f_everyc.htm
(every (lambda(x)(equal (mod x 2) 0)) '(2 4 6 7))

; https://stackoverflow.com/a/19653150
;(sb-ext:save-lisp-and-die "amazonas.exe" :toplevel #'main :executable t)
