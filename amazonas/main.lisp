
(defstruct pieza
   jugador
)

(defstruct posicion 
   x y 
)

(defun cambio-jugador(jugador)
  (case jugador
    (blanco 'negro)
    (negro 'blanco)))

(defun mostrar-celda (celda)
  (cond ((eq celda 'vacio) ".")
        ((eq celda 'flecha) "x")
        ((eq (pieza-jugador celda) 'blanco) "B")
        ((eq (pieza-jugador celda) 'negro) "N")))

(defun mostrar-tablero (tablero)
  (format t "  a b c d e f g h i j ~%~{~{~A~^ ~}~%~}~%"
    (let ((idx -1))
      (mapcar (lambda (x) (cons (setf idx (+ idx 1)) (mapcar 'mostrar-celda x))) tablero))))

(defparameter tablero-inicial '(
  (vacio vacio vacio #s(pieza :jugador negro) vacio vacio #s(pieza :jugador negro) vacio vacio vacio) 
  (vacio vacio vacio vacio vacio vacio vacio vacio vacio vacio)
  (vacio vacio vacio vacio vacio vacio vacio vacio vacio vacio)
  (#s(pieza :jugador negro) vacio vacio vacio vacio vacio vacio vacio vacio #s(pieza :jugador negro)) 
  (vacio vacio vacio vacio vacio vacio vacio vacio vacio vacio)
  (vacio vacio vacio vacio vacio vacio vacio vacio vacio vacio)
  (#s(pieza :jugador blanco) vacio vacio vacio vacio vacio vacio vacio vacio #s(pieza :jugador blanco)) 
  (vacio vacio vacio vacio vacio vacio vacio vacio vacio vacio)
  (vacio vacio vacio vacio vacio vacio vacio vacio vacio vacio)
  (vacio vacio vacio #s(pieza :jugador blanco) vacio vacio #s(pieza :jugador blanco) vacio vacio vacio) 
))

(defun actualizar-tablero (pos celda tablero)
  (let* ((old-fila (nth (posicion-x pos) tablero))
         (new-fila (append (subseq old-fila 0 (posicion-y pos)) (list celda) (subseq old-fila (+ (posicion-y pos) 1)))))
    (append (subseq tablero 0 (posicion-x pos)) (list new-fila) (subseq tablero (+ (posicion-x pos) 1)))))

(defun contenido-celda (pos tablero)
  (nth (posicion-y pos) (nth (posicion-x pos) tablero)))

(defun celda-es-vacia (pos tablero)
  (equal (contenido-celda pos tablero) 'vacio))

(defun mover-amazona (de hacia tablero)
  (actualizar-tablero hacia (contenido-celda de tablero) (actualizar-tablero de 'vacio tablero)))

(defun disparar-flecha (hacia tablero)
  (actualizar-tablero hacia 'flecha tablero))

(defun posiciones-pieza (celda tablero)
  (let 
    ((posiciones '()))
    (loop for x from 0 upto 9 do 
      (loop for y from 0 upto 9 do 
        (when 
          (equalp celda (contenido-celda (make-posicion :x x :y y) tablero)) 
          (setf posiciones (append posiciones (list (make-posicion :x x :y y)))))))
    posiciones))

(defun no-tiene-mov (pos tablero)
  (null (posibles-movs pos tablero)))

(defun jugador-no-tiene-mov (jugador tablero)
  (every (lambda (x) (no-tiene-mov x tablero)) (posiciones-pieza (make-pieza :jugador jugador) tablero)))

(defun es-pieza-del-jugador (pos jugador tablero)
  (equalp (contenido-celda pos tablero) (make-pieza :jugador jugador)))

(defun mov-permitido (pos1 pos2 tablero)
  (not (null 
    (find-if (lambda(x)(equalp x pos2)) (posibles-movs pos1 tablero)))))

(defun es-pos-valida (pos)
  (let ((x (posicion-x pos)) 
        (y (posicion-y pos)))
    (and (<= 0 x 9) (<= 0 y 9))))

(defun posibles-movs (pos tablero)
  (append 
    (movs-arriba pos tablero)
    (movs-arriba-der pos tablero)
    (movs-der pos tablero)
    (movs-abajo-der pos tablero)
    (movs-abajo pos tablero)
    (movs-abajo-izq pos tablero)
    (movs-izq pos tablero)
    (movs-arriba-izq pos tablero)))

(defun movs-der (pos tablero)
  (loop for i from (+ (posicion-y pos) 1) upto 9 
        until (not (celda-es-vacia (make-posicion :x (posicion-x pos) :y i) tablero)) 
        collect (make-posicion :x (posicion-x pos) :y i)))

(defun movs-izq (pos tablero)
  (loop for i from (- (posicion-y pos) 1) downto 0 
        until (not (celda-es-vacia (make-posicion :x (posicion-x pos) :y i) tablero)) 
        collect (make-posicion :x (posicion-x pos) :y i)))

(defun movs-arriba (pos tablero)
  (loop for i from (- (posicion-x pos) 1) downto 0 
        until (not (celda-es-vacia (make-posicion :x i :y (posicion-y pos)) tablero)) 
        collect (make-posicion :x i :y (posicion-y pos))))

(defun movs-abajo (pos tablero)
  (loop for i from (+ (posicion-x pos) 1) upto 9 
        until (not (celda-es-vacia (make-posicion :x i :y (posicion-y pos)) tablero)) 
        collect (make-posicion :x i :y (posicion-y pos))))

(defun movs-arriba-der (pos tablero)
  (cond ((or (eq (posicion-x pos) 0) (eq (posicion-y pos) 9)) nil)
        ((not (celda-es-vacia (make-posicion :x (- (posicion-x pos) 1) :y (+ (posicion-y pos) 1)) tablero)) nil)
        (t (append (list (make-posicion :x (- (posicion-x pos) 1) :y (+ (posicion-y pos) 1)))
                   (movs-arriba-der (make-posicion :x (- (posicion-x pos) 1) :y (+ (posicion-y pos) 1)) tablero)))))

(defun movs-arriba-izq (pos tablero)
  (cond ((or (eq (posicion-x pos) 0) (eq (posicion-y pos) 0)) nil)
        ((not (celda-es-vacia (make-posicion :x (- (posicion-x pos) 1) :y (- (posicion-y pos) 1)) tablero)) nil)
        (t (append (list (make-posicion :x (- (posicion-x pos) 1) :y (- (posicion-y pos) 1)))
                   (movs-arriba-izq (make-posicion :x (- (posicion-x pos) 1) :y (- (posicion-y pos) 1)) tablero)))))

(defun movs-abajo-der (pos tablero)
  (cond ((or (eq (posicion-x pos) 9) (eq (posicion-y pos) 9)) nil)
        ((not (celda-es-vacia (make-posicion :x (+ (posicion-x pos) 1) :y (+ (posicion-y pos) 1)) tablero)) nil)
        (t (append (list (make-posicion :x (+ (posicion-x pos) 1) :y (+ (posicion-y pos) 1)))
                   (movs-abajo-der (make-posicion :x (+ (posicion-x pos) 1) :y (+ (posicion-y pos) 1)) tablero)))))

(defun movs-abajo-izq (pos tablero)
  (cond ((or (eq (posicion-x pos) 9) (eq (posicion-y pos) 0)) nil)
        ((not (celda-es-vacia (make-posicion :x (+ (posicion-x pos) 1) :y (- (posicion-y pos) 1)) tablero)) nil)
        (t (append (list (make-posicion :x (+ (posicion-x pos) 1) :y (- (posicion-y pos) 1)))
                   (movs-abajo-izq (make-posicion :x (+ (posicion-x pos) 1) :y (- (posicion-y pos) 1)) tablero)))))

(defun mov-usuario (jugador tablero)
  (let (pos1)
    (setf pos1 (leer-posicion "Elija una pieza:"))
    (if (not (es-pieza-del-jugador pos1 jugador tablero))
      (progn (write-line "No es pieza del jugador") (mov-usuario jugador tablero))
      (let (pos2) 
        (setf pos2 (leer-posicion "Mover hacia:"))
        (if (not (mov-permitido pos1 pos2 tablero))
          (progn (write-line "Movimiento invalido") (mov-usuario jugador tablero))
          (let (pos3 tab1)
            (setf tab1 (mover-amazona pos1 pos2 tablero))
            (mostrar-tablero tab1)
            (setf pos3 (leer-posicion "Disparar flecha a:"))
            (if (not (mov-permitido pos2 pos3 tab1))
              (progn (write-line "Celda invalida") (mostrar-tablero tablero) (mov-usuario jugador tablero))
              (disparar-flecha pos3 tab1))))))))

(defun leer-posicion (msj)
  (let 
    (linea c1 c2 pos (pos-invalida (lambda() (progn (write-line "Posicion invalida. Use el formato 6a, 9d, etc.") (leer-posicion msj)))))
    (write-line msj)
    (setf linea (read-line))
    (if (and (equal (length linea) 2)
             (digit-char-p (setf c1 (char linea 0))) 
             (alpha-char-p (setf c2 (char linea 1))))
      (if (es-pos-valida (setf pos (make-posicion :x (parse-integer (string c1))
                                                  :y (- (char-code c2) 97))))
        pos
        (funcall pos-invalida))
      (funcall pos-invalida))))

(defun mov-aleatorio (jugador tablero)
  (let (amazns pos1 movs pos2 tab1 flechas pos3)
    (setf 
      amazns (remove-if (lambda (x) (no-tiene-mov x tablero)) (posiciones-pieza (make-pieza :jugador jugador) tablero))
      pos1 (nth (random (length amazns)) amazns)
      movs (posibles-movs pos1 tablero)
      pos2 (nth (random (length movs)) movs)
      tab1 (mover-amazona pos1 pos2 tablero)
      flechas (posibles-movs pos2 tab1)
      pos3 (nth (random (length flechas)) flechas))
    (disparar-flecha pos3 tab1)))

;; Usa primero el mejor, se elije el primero del resultado de ordernar-por-prioridad-movilidad
(defun mov-primero-el-mejor (jugador tablero)
  (let (tableros tablero-con-pos tablero-mejor-mov pos-amazona tableros-con-flecha)
    (loop for pos in (posiciones-pieza (make-pieza :jugador jugador) tablero) do 
      (setf tableros 
        ;; Le añadimos x(posicion) al ultimo elemento de la lista para saber la posicion de la amazona
        ;; puesto que se necesita para disparar la flecha luego.
        ;; Esta se ignora luego con la funcion butlast
        (append tableros (mapcar (lambda (x) (append (mover-amazona pos x tablero) (list x))) (posibles-movs pos tablero)))))
    (setf tablero-con-pos (car (ordernar-por-prioridad-movilidad jugador tableros))
          pos-amazona (car (last tablero-con-pos))
          tablero-mejor-mov (butlast tablero-con-pos))
    (butlast (car (ordernar-por-prioridad-movilidad
      jugador
      (mapcar (lambda (x) (append (disparar-flecha x tablero-mejor-mov) (list x))) (posibles-movs pos-amazona tablero-mejor-mov)))))))

(defun ordernar-por-prioridad-movilidad (jugador tableros-con-pos)
  (sort tableros-con-pos (lambda (tab1 tab2) 
     (> (prioridad-movilidad jugador (butlast tab1)) (prioridad-movilidad jugador (butlast tab2))))))

(defun prioridad-movilidad (jugador tablero)
  (let 
    ((amazns (posiciones-pieza (make-pieza :jugador jugador) tablero))
     (amazns-oponente (posiciones-pieza (make-pieza :jugador (cambio-jugador jugador)) tablero)))
    (- (sumatoria-movs amazns tablero) (sumatoria-movs amazns-oponente tablero))))

(defun sumatoria-movs (pos-amazns tablero)
  (if (null pos-amazns)
    0
    (+ (length (posibles-movs (car pos-amazns) tablero)) (sumatoria-movs (cdr pos-amazns) tablero))))

(defun bucle-juego (mov1 mov2 jugador tablero)
  (format t "~%Turno del jugador ~s ~%~%" jugador)
  (mostrar-tablero tablero)
  (let
    ((sig-jugador (cambio-jugador jugador)))
    (if (jugador-no-tiene-mov jugador tablero)
      sig-jugador
      (bucle-juego 
        mov1 
        mov2 
        sig-jugador 
        (case jugador 
          ('blanco (funcall mov1 jugador tablero))
          ('negro  (funcall mov2 jugador tablero)))))))

(defun main ()
  (let (ganador tipo-jugador-blanco tipo-jugador-negro)
    (setf *random-state* (make-random-state t)) ;; Necesario para el correcto funcionamiento del random
    (format t "~%~%EL JUEGO DE LAS AMAZONAS ~%~%")
    (setf tipo-jugador-blanco (leer-tipo-jugador 'blanco))
    (setf tipo-jugador-negro  (leer-tipo-jugador 'negro))
    (setf ganador (bucle-juego tipo-jugador-blanco tipo-jugador-negro 'blanco tablero-inicial))
    (format t "~% El jugador ~s gana!" ganador))
    (format t "~%~%Presione ENTER para cerrar")
    (read-line))

(defun leer-tipo-jugador (jugador)
  (let 
    (linea c1 c2 pos (opcion-invalida (lambda() (progn (write-line "Opcion invalida.") (leer-tipo-jugador jugador)))))
    (format t "~%Elija el tipo del jugador ~s~%" jugador)
    (format t "~%1)Humano")
    (format t "~%2)Maquina - Principiante(Aleatorio)")
    (format t "~%3)Maquina - Normal(Primero el mejor)")
    (format t "~%~%Opcion: ")
    (setf linea (read-line))
    (if (and (equal (length linea) 1)
             (digit-char-p (setf c1 (char linea 0))))
      (case (parse-integer (string c1))
        (1 'mov-usuario)
        (2 'mov-aleatorio)
        (3 'mov-primero-el-mejor)
        (otherwise (funcall opcion-invalida)))
      (funcall opcion-invalida))))

(main)
;(sb-ext:save-lisp-and-die "amazonas.exe" :toplevel #'main :executable t)
