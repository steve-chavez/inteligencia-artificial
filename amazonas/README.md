# El juego de las amazonas

Descomprima amazonas.zip. Luego, puede ejecutar el programa con amazonas-32.exe o amazonas-64.exe dependiendo de su versión de Windows.

Para ejecutar desde código fuente, descargue [sbcl](https://sourceforge.net/projects/sbcl/files/sbcl/1.4.2/sbcl-1.4.2-x86-windows-binary.msi/download?use_mirror=razaoinfo) y use el sig. comando en cmd:

```
sbcl --script main.lisp
```

La función evaluadora se obtuvo de los papers ubicados en la carpeta `/references`.
