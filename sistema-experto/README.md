## References

* https://github.com/topics/expert-system?l=prolog

* https://github.com/nicoladileo/NephroDoctor
  - Contains explanation module

* About proof search
  - http://learnprolognow.org/lpnpage.php?pagetype=html&pageid=lpn-htmlse6

* Machine learning
  - https://www.quora.com/Which-is-the-most-widely-used-language-for-Machine-Learning-in-industry
  - https://www.ibm.com/developerworks/community/blogs/jfp/entry/What_Language_Is_Best_For_Machine_Learning_And_Data_Science?lang=en
  - https://towardsdatascience.com/what-is-the-best-programming-language-for-machine-learning-a745c156d6b7
