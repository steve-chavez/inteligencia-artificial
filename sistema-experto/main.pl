% Ranking de lenguajes de programación segun distintos sitios web
% De https://www.tiobe.com/tiobe-index/
ranking(tiobe, java, 1).
ranking(tiobe, c, 2).
ranking(tiobe, cpp, 3).
ranking(tiobe, python, 4).
ranking(tiobe, visual_basic_net, 5).
ranking(tiobe, csharp, 6).
ranking(tiobe, javascript, 7).
ranking(tiobe, php, 8).
ranking(tiobe, sql, 9).
ranking(tiobe, go, 10).
ranking(tiobe, objectivec, 11).
ranking(tiobe, swift, 12).
ranking(tiobe, delphi, 13).
ranking(tiobe, r, 14).
ranking(tiobe, assembly, 15).
ranking(tiobe, ruby, 16).
ranking(tiobe, ruby, 16).
ranking(tiobe, matlab, 17).
ranking(tiobe, perl, 18).
ranking(tiobe, pl_sql, 19).
ranking(tiobe, visual_basic, 20).
ranking(tiobe, _, 0).

% De https://octoverse.github.com/projects#languages
ranking(github, javascript, 1).
ranking(github, java, 2).
ranking(github, python, 3).
ranking(github, php, 4).
ranking(github, cpp, 5).
ranking(github, csharp, 6).
ranking(github, typescript, 7).
ranking(github, bash, 8).
ranking(github, c, 9).
ranking(github, ruby, 10).
ranking(github, _, 0).

% De https://insights.stackoverflow.com/survey/2018/#technology-programming-scripting-and-markup-languages
ranking(stackoverflow, javascript, 1).
ranking(stackoverflow, html, 2).
ranking(stackoverflow, css, 3).
ranking(stackoverflow, sql, 4).
ranking(stackoverflow, java, 5).
ranking(stackoverflow, bash, 6).
ranking(stackoverflow, python, 7).
ranking(stackoverflow, csharp, 8).
ranking(stackoverflow, php, 9).
ranking(stackoverflow, cpp, 10).
ranking(stackoverflow, c, 11).
ranking(stackoverflow, typescript, 12).
ranking(stackoverflow, ruby, 13).
ranking(stackoverflow, swift, 14).
ranking(stackoverflow, assembly, 15).
ranking(stackoverflow, go, 16).
ranking(stackoverflow, objectivec, 17).
ranking(stackoverflow, visual_basic_net, 18).
ranking(stackoverflow, r, 19).
ranking(stackoverflow, matlab, 20).
ranking(stackoverflow, visual_basic, 21).
ranking(stackoverflow, kotlin, 22).
ranking(stackoverflow, scala, 23).
ranking(stackoverflow, groovy, 24).
ranking(stackoverflow, perl, 25).
ranking(stackoverflow, _, 0).

% De https://insights.stackoverflow.com/survey/2018/#top-paying-technologies
pago_promedio_anual(fsharp,      74000).
pago_promedio_anual(ocaml,       73000).
pago_promedio_anual(clojure,     72000).
pago_promedio_anual(groovy,      72000).
pago_promedio_anual(perl,        69000).
pago_promedio_anual(rust,        69000).
pago_promedio_anual(erlang,      67000).
pago_promedio_anual(scala,       67000).
pago_promedio_anual(go,          66000).
pago_promedio_anual(ruby,        64000).
pago_promedio_anual(bash,        63000).
pago_promedio_anual(cofeescript, 60000).
pago_promedio_anual(haskell,     60000).
pago_promedio_anual(julia,       60000).
pago_promedio_anual(typescript,  60000).
pago_promedio_anual(csharp,      59000).
pago_promedio_anual(objectivec,  58000).
pago_promedio_anual(r,           58000).
pago_promedio_anual(swift,       57000).
pago_promedio_anual(lua,         56000).
pago_promedio_anual(python,      56000).
pago_promedio_anual(sql,         56000).
pago_promedio_anual(javascript,  55000).
pago_promedio_anual(_,           0).

popularidad(X, Popularidad) :-
  ranking(tiobe, X, P1), ranking(github, X, P2), ranking(stackoverflow, X,P3),
  Popularidad is (P1 + P2 + P3)/3, !.

%lenguaje_mas_popular(X) :-

%lenguaje_mejor_pagado(X) :-

%lenguaje_mejor_pagado_acc(X, Acc) :-
  %X < Acc

main :-
  intro,
  reiniciar_respuestas,
  lenguaje(Lenguaje),
  describir(Lenguaje), nl.

intro :-
  write('Qué lenguaje de programación debería aprender primero?'), nl,
  write('Para responder, ingrese el número mostrado al costado de la respuesta, seguido por un punto (.)'), nl, nl.

:- dynamic(progreso/2).

reiniciar_respuestas :-
  retract(progreso(_, _)),
  fail.
reiniciar_respuestas.

lenguaje(python) :-
  por_que(para_hijos).

lenguaje(python) :-
  por_que(no_lo_se).

lenguaje(java) :-
  por_que(hacer_dinero),
  que_plataforma(no_importa).

lenguaje(cpp) :-
  por_que(hacer_dinero),
  que_plataforma(videojuegos).

lenguaje(objectivec) :-
  por_que(hacer_dinero),
  que_plataforma(so_movil),
  que_so_movil(ios).

lenguaje(java) :-
  por_que(hacer_dinero),
  que_plataforma(so_movil),
  que_so_movil(android).

lenguaje(python) :-
  por_que(hacer_dinero),
  que_plataforma(facebook).

lenguaje(python) :-
  por_que(hacer_dinero),
  que_plataforma(google).

lenguaje(csharp) :-
  por_que(hacer_dinero),
  que_plataforma(microsoft).

lenguaje(objectivec) :-
  por_que(hacer_dinero),
  que_plataforma(apple).

lenguaje(javascript) :-
  por_que(hacer_dinero),
  que_plataforma(web),
  web(frontend).

lenguaje(csharp) :-
  por_que(hacer_dinero),
  que_plataforma(web),
  web(backend),
  quiero_trabajar_para(empresarial),
  acerca_de_microsoft(soy_fan).

lenguaje(java) :-
  por_que(hacer_dinero),
  que_plataforma(web),
  web(backend),
  quiero_trabajar_para(empresarial),
  acerca_de_microsoft(no_esta_mal).

lenguaje(java) :-
  por_que(hacer_dinero),
  que_plataforma(web),
  web(backend),
  quiero_trabajar_para(empresarial),
  acerca_de_microsoft(son_malos).

lenguaje(javascript) :-
  por_que(hacer_dinero),
  que_plataforma(web),
  web(backend),
  quiero_trabajar_para(startup),
  intentar_algo_nuevo(si).

lenguaje(python) :-
  por_que(hacer_dinero),
  que_plataforma(web),
  web(backend),
  quiero_trabajar_para(startup),
  intentar_algo_nuevo(no),
  juguete_favorito(lego).

lenguaje(ruby) :-
  por_que(hacer_dinero),
  que_plataforma(web),
  web(backend),
  quiero_trabajar_para(startup),
  intentar_algo_nuevo(no),
  juguete_favorito(play_doh).

lenguaje(php) :-
  por_que(hacer_dinero),
  que_plataforma(web),
  web(backend),
  quiero_trabajar_para(startup),
  intentar_algo_nuevo(no),
  juguete_favorito(viejo).

lenguaje(csharp) :-
  por_que(hacer_dinero),
  que_plataforma(empresarial),
  acerca_de_microsoft(soy_fan).

lenguaje(java) :-
  por_que(hacer_dinero),
  quiero_trabajar_para(empresarial),
  acerca_de_microsoft(no_esta_mal).

lenguaje(java) :-
  por_que(hacer_dinero),
  quiero_trabajar_para(empresarial),
  acerca_de_microsoft(son_malos).

lenguaje(python) :-
  por_que(solo_por_diversion),
  prefiero_aprender(manera_facil).

lenguaje(python) :-
  por_que(solo_por_diversion),
  prefiero_aprender(mejor_manera).

lenguaje(java) :-
  por_que(solo_por_diversion),
  prefiero_aprender(forma_dificil),
  carro(auto).

lenguaje(c) :-
  por_que(solo_por_diversion),
  prefiero_aprender(forma_dificil),
  carro(manual).

lenguaje(cpp) :-
  por_que(solo_por_diversion),
  prefiero_aprender(manera_mas_dificil).

lenguaje(python) :-
  por_que(me_interesa),
  prefiero_aprender(manera_facil).

lenguaje(python) :-
  por_que(me_interesa),
  prefiero_aprender(mejor_manera).

lenguaje(java) :-
  por_que(me_interesa),
  prefiero_aprender(forma_dificil),
  carro(auto).

lenguaje(c) :-
  por_que(me_interesa),
  prefiero_aprender(forma_dificil),
  carro(manual).

lenguaje(cpp) :-
  por_que(me_interesa),
  prefiero_aprender(manera_mas_dificil).

lenguaje(python) :-
  por_que(mejora_personal),
  prefiero_aprender(manera_facil).

lenguaje(python) :-
  por_que(mejora_personal),
  prefiero_aprender(mejor_manera).

lenguaje(java) :-
  por_que(mejora_personal),
  prefiero_aprender(forma_dificil),
  carro(auto).

lenguaje(c) :-
  por_que(mejora_personal),
  prefiero_aprender(forma_dificil),
  carro(manual).

lenguaje(cpp) :-
  por_que(mejora_personal),
  prefiero_aprender(manera_mas_dificil).

pregunta(por_que) :-
  write('Por qué quieres aprender a programar?'), nl.

pregunta(que_plataforma) :-
  write('Qué plataforma/campo?'), nl.

pregunta(que_so_movil) :-
  write('Qué sistema operativo?'), nl.

pregunta(web) :-
  write('Backend o Frontend?'), nl.

pregunta(quiero_trabajar_para) :-
  write('Quiero trabajar para...'), nl.

pregunta(acerca_de_microsoft) :-
  write('Qué piensas de Microsoft?'), nl.

pregunta(intentar_algo_nuevo) :-
  write('Quieres intentar algo nuevo, con gran potencial, pero menos maduro?'), nl.

pregunta(juguete_favorito) :-
  write('Cuál es tu juguete favorito?'), nl.

pregunta(prefiero_aprender) :-
  write('Prefiero aprender las cosas...'), nl.

pregunta(carro) :-
  write('Carro automático o manual?'), nl.

respuesta(para_hijos) :-
  write('Para recomendarlo a mis hijos').

respuesta(no_lo_se) :-
  write('No lo sé').

respuesta(hacer_dinero) :-
  write('Hacer dinero').

respuesta(solo_por_diversion) :-
  write('Solo por diversión').

respuesta(me_interesa) :-
  write('Me interesa').

respuesta(mejora_personal) :-
  write('Mejora personal').

respuesta(no_importa) :-
  write('No importa, solo quiero $$$').

respuesta(videojuegos) :-
  write('Videojuegos').

respuesta(so_movil) :-
  write('Sistema operativo movil').

respuesta(facebook) :-
  write('Facebook').

respuesta(google) :-
  write('Google').

respuesta(microsoft) :-
  write('Microsoft').

respuesta(apple) :-
  write('Apple').

respuesta(web) :-
  write('Web').

respuesta(empresarial) :-
  write('Mercado empresarial').

respuesta(ios) :-
  write('iOS').

respuesta(android) :-
  write('Android').

respuesta(frontend) :-
  write('Frontend (interfaz web)').

respuesta(backend) :-
  write('Backend ("cerebro" detrás de aplicación web)').

respuesta(startup) :-
  write('Startup').

respuesta(soy_fan) :-
  write('Soy fanático!').

respuesta(no_esta_mal) :-
  write('No está mal').

respuesta(son_malos) :-
  write('Son malos').

respuesta(si) :-
  write('Si').

respuesta(no) :-
  write('No').

respuesta(lego) :-
  write('Lego').

respuesta(play_doh) :-
  write('Play-Doh').

respuesta(viejo) :-
  write('Tengo un juguete viejo y no lo cambiaría').

respuesta(manera_facil) :-
  write('La manera facil').

respuesta(mejor_manera) :-
  write('La mejor manera').

respuesta(forma_dificil) :-
  write('La manera difícil').

respuesta(manera_mas_dificil) :-
  write('La manera más difícil').

respuesta(auto) :-
  write('Auto').

respuesta(manual) :-
  write('Manual').


describir(python) :-
  write('Python'), nl,
  write('Reconocido como el mejor lenguaje de programación para principiantes'), nl,
  write('Más fácil de aprender').

describir(java) :-
  write('Java'), nl,
  write('Uno de los lenguajes de programación con más demanda y mejor paga'), nl,
  write('Eslogan: una vez programado funciona en cualquier entorno').

describir(c) :-
  write('C'), nl,
  write('La lingua franca de los lenguajes de programación'), nl,
  write('El lenguaje más usado en producción en el mundo').

describir(cpp) :-
  write('C++'), nl,
  write('Versión más compleja de C con muchas más funcionalidades'), nl,
  write('Solo recomendado si es que se tiene un guía').

describir(javascript) :-
  write('JavaScript'), nl,
  write('El lenguaje más importante para la web'), nl,
  write('De obligatorio aprendizaje para el frontend').

describir(csharp) :-
  write('C#'), nl,
  write('Una opción popular para aplicaciones empresariales y nativas de Windows'), nl,
  write('Similar a Java').

describir(ruby) :-
  write('Ruby'), nl,
  write('Reconocido por su popular framework, Ruby on Rails'), nl,
  write('Se enfoca en hacer las cosas rápido').

describir(php) :-
  write('PHP'), nl,
  write('Adecuado para simples y pequeños sitios web implementados con poco tiempo de desarrollo'), nl,
  write('Soportado por casi todo hosting y a bajo precio').

describir(objectivec) :-
  write('Objective-C'), nl,
  write('Lenguaje prinicipal usado por Apple para MacOSX y iOS'), nl,
  write('Elije esto si es que quieres enfocarte en aplicaciones nativas para el ecosistema Apple').


por_que(Rpta) :-
  progreso(por_que, Rpta).
por_que(Rpta) :-
  \+ progreso(por_que, _),
  preguntar(por_que, Rpta, [para_hijos, no_lo_se, hacer_dinero, solo_por_diversion, me_interesa, mejora_personal]).

que_plataforma(Rpta) :-
  progreso(que_plataforma, Rpta).
que_plataforma(Rpta) :-
  \+ progreso(que_plataforma, _),
  preguntar(que_plataforma, Rpta, [no_importa, videojuegos, so_movil, facebook, google, microsoft, apple, web, empresarial]).

que_so_movil(Rpta) :-
  progreso(que_so_movil, Rpta).
que_so_movil(Rpta) :-
  \+ progreso(que_so_movil, _),
  preguntar(que_so_movil, Rpta, [ios, android]).

web(Rpta) :-
  progreso(web, Rpta).
web(Rpta) :-
  \+ progreso(web, _),
  preguntar(web, Rpta, [frontend, backend]).

quiero_trabajar_para(Rpta) :-
  progreso(quiero_trabajar_para, Rpta).
quiero_trabajar_para(Rpta) :-
  \+ progreso(quiero_trabajar_para, _),
  preguntar(quiero_trabajar_para, Rpta, [startup, empresarial]).

acerca_de_microsoft(Rpta) :-
  progreso(acerca_de_microsoft, Rpta).
acerca_de_microsoft(Rpta) :-
  \+ progreso(acerca_de_microsoft, _),
  preguntar(acerca_de_microsoft, Rpta, [soy_fan, no_esta_mal, son_malos]).

intentar_algo_nuevo(Rpta) :-
  progreso(intentar_algo_nuevo, Rpta).
intentar_algo_nuevo(Rpta) :-
  \+ progreso(intentar_algo_nuevo, _),
  preguntar(intentar_algo_nuevo, Rpta, [si, no]).

juguete_favorito(Rpta) :-
  progreso(juguete_favorito, Rpta).
juguete_favorito(Rpta) :-
  \+ progreso(juguete_favorito, _),
  preguntar(juguete_favorito, Rpta, [lego, play_doh, viejo]).

prefiero_aprender(Rpta) :-
  progreso(prefiero_aprender, Rpta).
prefiero_aprender(Rpta) :-
  \+ progreso(prefiero_aprender, _),
  preguntar(prefiero_aprender, Rpta, [manera_facil, mejor_manera, forma_dificil, manera_mas_dificil]).

carro(Rpta) :-
  progreso(carro, Rpta).
carro(Rpta) :-
  \+ progreso(carro, _),
  preguntar(carro, Rpta, [auto, manual]).

respuestas([], _).
respuestas([Primero|Resto], Indice) :-
  write(Indice), write(' '), respuesta(Primero), nl,
  SigIndice is Indice + 1,
  respuestas(Resto, SigIndice).

preguntar(Pregunta, Rpta, Alternativas) :-
  pregunta(Pregunta),
  respuestas(Alternativas, 0),
  read(Indice),
  nth0(Indice, Alternativas, Resultado),
  asserta(progreso(Pregunta, Resultado)),
  Resultado = Rpta.
